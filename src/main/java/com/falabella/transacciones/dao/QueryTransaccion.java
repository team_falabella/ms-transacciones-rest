package com.falabella.transacciones.dao;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;
import utils.SqlReader;

@Repository
public class QueryTransaccion {
	private static final Logger log = LoggerFactory.getLogger(QueryTransaccion.class);
	private final NamedParameterJdbcTemplate template;

	@Autowired
	public QueryTransaccion(NamedParameterJdbcTemplate template) {
		this.template = template;
	}

	public <T> Flux<T> findObjectsArray(String sqlPath, Map<String, Object> params, Class<T> clazz) {
		String dynamicSql = SqlReader.readSql(sqlPath);
		if (params.get("fechaDesde") != null && params.get("fechaHasta") != null) {
			dynamicSql += "and  (transaccion.LHTOFI between " + params.get("fechaDesde") + " and "
					+ params.get("fechaHasta") + ")";
		}
		if (params.get("horaDesde") != null && params.get("horaHasta") != null) {
			dynamicSql += "and (transaccion.LHTOHI between " + params.get("horaDesde") + " and "
					+ params.get("horaHasta") + ")";
		}
		if (params.get("bin") != null && params.get("ultimosDigitos") != null) {
			dynamicSql += "AND SUBSTR (transaccion.LHTINT, 1, 6) = '" + params.get("bin")
					+ "' AND SUBSTR (transaccion.LHTINT, 7, 4) = '" + params.get("ultimosDigitos") + "'";
		}
		log.info(dynamicSql);
		String sql = dynamicSql;
		return Flux.defer(() -> Flux.fromIterable(template.query(sql, params, new BeanPropertyRowMapper<>(clazz))))
				.subscribeOn(Schedulers.elastic());
	}
}
