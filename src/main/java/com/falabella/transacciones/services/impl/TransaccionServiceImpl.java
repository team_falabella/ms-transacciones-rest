package com.falabella.transacciones.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.falabella.transacciones.dao.QueryTransaccion;
import com.falabella.transacciones.models.Transaccion;
import com.falabella.transacciones.services.TransaccionService;

import utils.IbmCommandCall;

@Service
public class TransaccionServiceImpl implements TransaccionService {

	private final QueryTransaccion queryDao;

	@Autowired
	public TransaccionServiceImpl(QueryTransaccion queryDao) {
		this.queryDao = queryDao;
	}

	private static final String SQLGETTRANSACCIONES = "/sql/transacciones/get/getTransacciones.sql";

	@Override
	public List<Transaccion> getTransacciones(Integer fechaDesde, Integer fechaHasta, Integer horaDesde,
			Integer horaHasta, String bin, String ultimosDigitos, String rut) {
		Map<String, Object> params = new HashMap<>();
		params.put("fechaDesde", fechaDesde);
		params.put("fechaHasta", fechaHasta);
		params.put("horaDesde", horaDesde);
		params.put("horaHasta", horaHasta);
		params.put("bin", bin);
		params.put("ultimosDigitos", ultimosDigitos);
		params.put("rut", rut);
		List<Transaccion> transacciones = queryDao.findObjectsArray(SQLGETTRANSACCIONES, params, Transaccion.class)
				.collectList().block();
		Map<String, Object> tarjetas = new HashMap<>();
		transacciones.stream().filter(distinctByKey(Transaccion::getNumeroTarjetaTransaccion)).forEach(t -> tarjetas
				.put(t.getNumeroTarjetaTransaccion(), IbmCommandCall.callVpan(t.getNumeroTarjetaTransaccion())));
		transacciones.stream().forEach(t -> {
				try{
					t.setNumeroTarjetaTransaccion(tarjetas.get(t.getNumeroTarjetaTransaccion()).toString().split("TJ:")[1].trim());	
				}catch(Throwable e){
					t.setNumeroTarjetaTransaccion("Sin información");
				}});
		return transacciones;
	}

	public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
		Set<Object> seen = ConcurrentHashMap.newKeySet();
		return t -> seen.add(keyExtractor.apply(t));
	}

}
